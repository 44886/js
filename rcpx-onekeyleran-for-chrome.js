/*
 * 重庆公需科目一键学习js
 * */
(function(d) {
	var load = {};
	//以下代码用来动态加载任何js代码
	load.getScript = function(filename, func) {
		var script = document.createElement('script');
		script.setAttribute("type", "text/javascript");
		script.setAttribute("src", filename);
		script.addEventListener("load", func);
		if (typeof script != "undefined");
		document.getElementsByTagName("head")[0].appendChild(script);
	};
	url = window.location.href;

	function run() {
		$(function() {

			/*一键学习*/
			if (url.substr(0, 67) == "http://zx.rcpx.cc/els/html/courseStudyItem/courseStudyItem.learn.do") {
				/*增加一键学习按钮*/
				$("body").append('<button class="oneKeyLearn">一键学完</button>');
				/*当一个页面上有视频列表的时候，切换视频会更换scoId，本功能是获取最新的scoId*/
				$("body").on("click", ".play-btn", function() {
					window.scoId = $(this).attr("data-id");
				});
				$("body").on("click", ".oneKeyLearn", function() {
					var scoId = window.scoId;
					var courseId = $("#courseId").val();
					var elsSign = getCookie('eln_session_id');
					var url = "http://zx.rcpx.cc/els/html/courseStudyItem/courseStudyItem.saveProgress.do";
					/*
					 *根据不同的课程，构造不同的数据
					 * */
					if ($("#viewerPlaceHolder").length > 0) {
						/*全屏视频*/
						$.ajax({
							url: "http://zx.rcpx.cc/els/html/courseStudyItem/courseStudyItem.saveCoursePrecent.do",
							data: "courseId=" + courseId + "&playTime=6000",
							type: "POST",
							success: function(msg) {
								console.log(msg);
								if (msg.courseProgress == "100") {
									alert("已经学完，嘿嘿");
								} else {
									alert("学习失败，也可能是这个已经学完过了。");
								}
							},
							error: function(e) {
								alert("出现错误,可能不支持这种视频样式");
								console.log(e);
							}
						});
						return;
					}
					/*第2屏幕*/
					if (window.info.courseStandard == "TWOSCREEN") {

						$.ajax({
							url: url,
							data: "courseId=" + courseId + "&scoId=" + scoId + "&progress_measure=100&session_time=120:00&logId=&location=800&elsSign=" + elsSign,
							type: "POST",
							dataType: "json",
							success: function(msg) {
								console.log(msg);
								if (msg.completed == "true") {
									alert("已经学完，嘿嘿");
								} else {
									alert("学习失败，也可能是这个已经学完过了。");
								}
							},
							error: function(e) {
								alert("出现错误,可能不支持这种视频样式");
								console.log(e);
							}
						});

					}
					/*第3屏幕*/
					if (window.info.courseStandard == "THREESCREEN") {
						alert("抱歉，暂时不支持这种视频，我们还在破解！");
						return false;
						$.ajax({
							url: url,
							data: "courseId=" + courseId + "&scoId=" + scoId + "&progress_measure=100&session_time=30:00&location=1843.4",
							type: "POST",
							dataType: "json",
							beforeSend: function(request) {
								request.setRequestHeader("Referer", "http://zx.rcpx.cc/els/flash/elnFlvPlayer.swf?v=5");
							},
							headers: {
								Origin: 'http://ditu.google.cn',
								Referer: "http://zx.rcpx.cc/els/flash/elnFlvPlayer.swf?v=5",
								"a": "b"
							},
							success: function(msg) {
								console.log(msg);
								if (msg.completed == "true") {
									alert("已经学完，嘿嘿");
								} else {
									alert("学习失败，也可能是这个已经学完过了。");
								}
							},
							error: function(e) {
								alert("出现错误,可能不支持这种视频样式");
								console.log(e);
							}
						});

					}
				});
			}
			/*奉献答案*/
			if (url.substr(0, 52) == "http://zx.rcpx.cc/ems/html/myExam/myExamPaperView.do") {
				/*创建按钮*/
				$("body").append('<button class="uploudAnswer">共享答案</button>');
				$("body").on("click", ".uploudAnswer", function() {
					/*移除错题*/
					$(".myexa-wrong").each(function() {
						$($(this).children("a").attr("href")).remove();
					});
					/*获取正确答案*/
					var answer = {};
					$('[checked="checked"]').each(function() {
						answer[$(this).attr("id")] = "checked";
						$(this).remove();
					});
					$('[type="radio"],[checked="checked"]').each(function() {
						answer[$(this).attr("id")] = "notChecked";
						$(this).remove();
					});
					var cData = new Wilddog("https://rcpx-chrome2.wilddogio.com/" + GetQueryString("examId") + "/");
					cData.update(answer, function(err) {
						if (err) {
							console.log(err);
							alert("共享答案失败！");
						} else {
							console.log("共享答案成功！");
							alert("共享答案成功！");
						}
					});

				});

			}
			/*一键答题*/
			if (url.substr(0, 48) == "http://zx.rcpx.cc/ems/html/myExam/myExamPaper.do") {
				/*创建按钮*/
				$("body").append('<button class="oneKeyAnswer">一键答题</button>');
				$("body").on("click", ".oneKeyAnswer", function() {

					var cData = new Wilddog("https://rcpx-chrome2.wilddogio.com/" + GetQueryString("examId") + "/");
					cData.once("value", function(data) {
						for (i in data.val()) {
							if (data.val()[i] == "checked") {
								$("#" + i).attr("checked", "checked");
								var num = $("#" + i).parent().parent().parent().find(".myexa-que-num").html();
								var question = "myexa_quest_" + num;
								/*标注绿色*/
								$('[href="#' + question + '"]').parent().addClass("myexa-has-answer");
							}
						}
						alert("答案标注完成，如果还有题末标注，说明题库中还没有，请自行百度！\r\n 提交试卷后，请记得查看自己的答案，然后共享！");
					}, function(errorObject) {
						alert("从答案库里获取答案失败！");
					});
				});

			}
		});
	}

	/*正则提取 cookie*/
	function getCookie(name) {
		var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
		if (arr = document.cookie.match(reg)) {
			return unescape(arr[2]);
		} else {
			return null;
		}
	}
	/*获取地址中的参数*/
	function GetQueryString(name) {
		var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
		var r = window.location.search.substr(1).match(reg);
		if (r != null) return unescape(r[2]);
		return null;
	}
	/*将页面变量放入dom中*/
	function getscoId() {
		var e = document.createElement("script");
		e.innerHTML = " var newDiv = document.createElement('div'); newDiv.id='scoId'; newDiv.style='display:none';newDiv.innerHTML=scoId; document.body.appendChild(newDiv) ";
		document.body.appendChild(e);
	}

	/*可能会加载很多库，所以自定义这个对象*/
	if (typeof($) == undefined) {
		load.getScript("http://libs.baidu.com/jquery/1.9.1/jquery.min.js", run);
	} else {
		run();
	}
	/*加载野狗*/
	load.getScript("https://cdn.wilddog.com/js/client/current/wilddog.js", function() {});
})(document);