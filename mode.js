/*
 * 书签js一键操作模板
 * 本代码能在没有jquery的网页先加载jquery
 * 正式的代码在else{}中
 * */
(function(d) {
	var load = {};
	//以下代码用来动态加载任何js代码
	load.getScript = function(filename,func) {
		var script = document.createElement('script');
		script.setAttribute("type", "text/javascript");
		script.setAttribute("src", filename);
		script.addEventListener("load",func);
		if (typeof script != "undefined");
		document.getElementsByTagName("head")[0].appendChild(script);
	};

    function run(){
    	/*这是正式的代码*/
    }

	/*可能会加载很多库，所以自定义这个对象*/
	load.getScript("http://libs.baidu.com/jquery/1.9.1/jquery.min.js",run);

})(document);